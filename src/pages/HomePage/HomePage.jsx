import {useState } from "react";
import Modal from "../../components/Modal/Modal"
import ProductList from "../../components/Product/components/ProductList/ProductList";

function HomePage(props) {

    const [firstModalOpen, setFirstModalOpen] = useState(false);
	const [selectedProduct, setSelectedProduct] = useState(false);

	const openFirstModal = (product) => {
		setFirstModalOpen(true);
		setSelectedProduct(product);
	};
	
	const closeFirstModal = () => {
		setFirstModalOpen(false);
		setSelectedProduct(false);
    };
   
    const addToCart = () => {
        props.setCartItems(prevCartItems => [...prevCartItems, selectedProduct]);
	};

    return ( 
        <>
			<ProductList openFirstModal= {openFirstModal}
			 addToFavorites={props.addToFavorites} products={props.products} />

			<Modal
                header="Додати цей товар у кошик?"
                isOpen={firstModalOpen}
                isClose={closeFirstModal}
                text={selectedProduct.title}
                actions={[
                    {
                        label: 'Ok',
						onClick: () => {
							addToCart()
							closeFirstModal()
						},						
						backgroundColor: 'deepskyblue'
                    },
                    {
                        label: 'Cancel',
						onClick: () => closeFirstModal(),
						backgroundColor: 'gray'
                    }
                ]}
            />
        </>
     );
}

export default HomePage;