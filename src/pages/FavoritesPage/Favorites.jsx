import {useState } from "react";
import FavoritesList from "../../components/Product/components/FavoritesList/FavoritesList";
import Modal from "../../components/Modal/Modal"

function Favorites(props) {

	const [firstModalOpen, setFirstModalOpen] = useState(false);
	const [selectedProduct, setSelectedProduct] = useState(false);

	const openFirstModal = (product) => {
		setFirstModalOpen(true);
		setSelectedProduct(product);
	};
	
	const closeFirstModal = () => {
		setFirstModalOpen(false);
		setSelectedProduct(false);
    };
    
    const addToCart = () => {
        props.setCartItems(prevCartItems => [...prevCartItems, selectedProduct]);
	};

    return ( 
        <>
            <FavoritesList openFirstModal= {openFirstModal}
                           addToFavorites= {props.addToFavorites}
                           favorites= {props.favorites}/>
			<Modal
                header="Додати цей товар у кошик?"
                isOpen={firstModalOpen}
                isClose={closeFirstModal}
                text={selectedProduct.title}
                actions={[
                    {
                        label: 'Ok',
						onClick: () => {
							addToCart()
							closeFirstModal()
						},						
						backgroundColor: 'deepskyblue'
                    },
                    {
                        label: 'Cancel',
						onClick: () => closeFirstModal(),
						backgroundColor: 'gray'
                    }
                ]}
            />			   
        </>
     );
}

export default Favorites;