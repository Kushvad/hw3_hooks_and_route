
import CartList from "../../components/Product/components/CartList/CartList";
import Modal from "../../components/Modal/Modal";
import {useState } from "react";

function Cart(props) {

    const [deleteModalOpen, setDeleteModalOpen] = useState(false);
    const [selectedProduct, setSelectedProduct] = useState(false);

	const openDeleteModal = (product) => {
		setDeleteModalOpen(true);
		setSelectedProduct(product);
	};
	
	const closeDeleteModal = () => {
		setDeleteModalOpen(false);
		setSelectedProduct(false);
    };

    const deleteFromCart = (productId) => {
        const updatedCartItems = props.cartItems.filter(item => item.id !== productId);
		props.setCartItems(updatedCartItems);
		localStorage.setItem("cart", JSON.stringify(updatedCartItems));
	};

    return ( 
        <>
            <CartList openDeleteModal= {openDeleteModal}
                        cartItems= {props.cartItems}/>
             <Modal
                header="Видалити цей товар з кошика?"
                isOpen={deleteModalOpen}
                isClose={closeDeleteModal}
                text={selectedProduct.title}
                actions={[
                    {
                        label: 'Ok',
						onClick: () => {
							deleteFromCart(selectedProduct.id)
							closeDeleteModal()
						},						
						backgroundColor: 'red'
                    },
                    {
                        label: 'Cancel',
						onClick: () => closeDeleteModal(),
						backgroundColor: 'gray'
                    }
                ]}
            />
        </>
     );
}

export default Cart;
