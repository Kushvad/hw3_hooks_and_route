import Header from "../Header/Header";

function PageContainer({children, cartsCount, favoritesCount}) { 
    return(
        <div className="main">
            <Header
				cartsCount={cartsCount}
				favoritesCount={favoritesCount}
			/>

            {children}
        </div>
    )
}

export default PageContainer;