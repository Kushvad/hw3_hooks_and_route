// import { useState, useEffect } from 'react'
import  './Header.scss'
import {ReactComponent as Logo} from "./icons/logo.svg";
import {ReactComponent as Basket} from "./icons/basket-shopping-solid.svg";
import {ReactComponent as Star} from "./icons/star-solid.svg";
import { Link, useLocation } from 'react-router-dom';
import cn from 'classnames'

function Header({cartsCount, favoritesCount}) {

    const {pathname} = useLocation;
    
    return(
        <header className="header">
            <div className="container">
                <Link to="/"><Logo/></Link>
                <div className="icon-container">
                    <Link to="/cart" className={cn("icon-header", {active:pathname === "/cart"})}><Basket/></Link>
                    <span>{cartsCount}</span>    
                    <Link to="/favorites" className={cn("icon-header", {active:pathname === "/favorites"})}><Star/></Link>
                    <span>{favoritesCount}</span>
                </div>
            </div>
            
        </header>
    )
}

export default Header;