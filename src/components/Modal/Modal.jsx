import  './Modal.scss'
import {ReactComponent as Close} from "./icons/close.svg";
import PropTypes from 'prop-types'

function Modal(props) {
    const {header, isOpen, isClose, text, actions} = props;
    
    if (!isOpen) return null;

    return (
        <div className="modalBack" onClick={isClose}>
            <div className="modal" onClick={(e) => e.stopPropagation()}>
                <div className="modal-header">
                    <h1 className="title">{header}</h1>                
                    <button className="closeButton"  onClick={isClose}>
                        <Close/>
                    </button>
                </div>
                <p className="text">{text}</p>
                <div className="buttonWrapper">

                    {actions.map((action, index) => (
                        <button className="actionButton" style={{ backgroundColor: action.backgroundColor }} key={index} onClick={action.onClick}>{action.label}</button>
                    ))}
                </div>                
            </div>
        </div>       
    )
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    isOpen: PropTypes.bool,
    isClose: PropTypes.func,
    text: PropTypes.string,
    actions: PropTypes.array
}

export default Modal;