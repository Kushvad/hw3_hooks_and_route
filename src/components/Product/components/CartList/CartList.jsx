
import CartProduct from './CartProduct/CartProduct';
import '../Product.scss'


function FavoritesList(props){
    
    return(
        <ul className="list">
        {props.cartItems.map(product =>
            <CartProduct
                title={product.title}
                price={product.price}
                number={product.number}
                id={product.id}
                key={product.id}
                image={product.image}
                openModal={() => props.openDeleteModal(product)}
            />
        )}
    </ul>
    )
}

export default FavoritesList;