import { useState, useEffect } from 'react';
import Product from './Product/Product';
import '../Product.scss'

function ProductList(props){

    return(
        <ul className="list">
        {props.products.map(product =>
            <Product
                title={product.title}
                price={product.price}
                number={product.number}
                id={product.id}
                key={product.id}
                image={product.image}
                openModal={() => props.openFirstModal(product)}
                addToFavorites={() => props.addToFavorites(product)}
            />
        )}
    </ul>
    )
}

export default ProductList;