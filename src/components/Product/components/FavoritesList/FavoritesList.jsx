import CartProduct from './FavoritesProduct/FavoriteProduct'
import '../Product.scss'


function FavoritesList(props){
    

    return(
        <ul className="list">
        {props.favorites.map(product =>
            <CartProduct
                title={product.title}
                price={product.price}
                number={product.number}
                id={product.id}
                key={product.id}
                image={product.image}
                openModal={() => props.openFirstModal(product)}
				addToFavorites={() => props.addToFavorites(product)}
            />
        )}
    </ul>
    )
}

export default FavoritesList;