
import { Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import Cart from "./pages/CartPage/Cart";
import Favorites from "./pages/FavoritesPage/Favorites";
import  "./App.css"
import { useEffect, useState } from "react";
import PageContainer from "./components/PageContainer/PageContainer";

function App() {
	const [products, setProducts] = useState([])
    useEffect(() => {
		const url = "./products.json"
        fetch(url).then(resp => resp.json()).then(data => setProducts(data.products))
	}, [])


	const [cartItems, setCartItems] = useState([]);

	useEffect(() => {
        const cart = JSON.parse(localStorage.getItem("cart")) || [];
        setCartItems(cart);
		const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
        setFavorites(favorites);
    }, []);

	useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cartItems));
	}, [cartItems]);

	
	const [favorites, setFavorites] = useState([]);

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
	}, [favorites]);

		
	const [favoritesCount, setFavoritesCount] = useState();

	useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem("favorites")|| []);
        setFavoritesCount(favorites.length);
	}, [favorites]);


	const addToFavorites = (product) => {
		const updatedProduct = { ...product, isFavorite: !product.isFavorite };

		setFavorites(prevFavorites => {
			const existingProductIndex = prevFavorites.findIndex(item => item.id === updatedProduct.id);

			if (existingProductIndex !== -1) {
				prevFavorites.splice(existingProductIndex, 1);
			} else {
				prevFavorites.push(updatedProduct);
			}

			return [...prevFavorites];
		});
	};

	return (
		<PageContainer cartsCount={cartItems.length}
		               favoritesCount={favoritesCount}>
		<Routes>
			<Route index element={<HomePage products={products} 
			                                setCartItems={setCartItems} 
											addToFavorites={addToFavorites}/>}/>
			<Route path="/" element={<HomePage/>}/>
			<Route path="/cart" element={<Cart cartItems={cartItems}
			                                   setCartItems={setCartItems}/>}/>
			<Route path="/favorites" element={<Favorites favorites={favorites}
														 addToFavorites={addToFavorites}/>}/>
		</Routes>
		</PageContainer>	
	)
}

export default App;
